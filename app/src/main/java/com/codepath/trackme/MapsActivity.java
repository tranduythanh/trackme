package com.codepath.trackme;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.DialogInterface;

import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.TextView;
import android.widget.Toast;
import android.app.AlertDialog;

import com.chimeraiot.android.ble.BleScanCompat;
import com.chimeraiot.android.ble.BleUtils;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;

import java.util.HashMap;
import java.util.Map;

public class MapsActivity extends FragmentActivity
        implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener{

    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    public String TAG = "customlog-";
    private boolean mRequestingLocationUpdates;
    private boolean isDisableLocationUpdate;
    private BleScanCompat scanner;
    private static final long SCAN_PERIOD = 2000L;
    LocationRequest mLocationRequest;
    BluetoothAdapter bluetoothAdapter;
    double range = 0;

    private final Map<BluetoothDevice, Integer> rssiMap = new HashMap<>();

    private static int appState;
    TextView tvStatus;
    TextView tvLargeText;
    private static final int BLE_DISCONECTED_STATE = 0;
    private static final int CONNECTING_STATE = 1;
    private static final int BLE_TRACKING_STATE = 2;
    private static final int REQUEST_ENABLE_BT = 1;
    private static final String RECORD_DEVICE_NAME = "SensorTag";

    private Firebase mRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        tvStatus = (TextView) findViewById(R.id.tvStatus);
        tvLargeText = (TextView) findViewById(R.id.tvLargeText);

        mapFragment.getMapAsync(this);
        createGoogleApiClient();

        checkAndInitBluetoothAdapter();

        turnBluetoothOn();

        if (bluetoothAdapter == null) {
            return;
        }

        updateViewStatus(BLE_DISCONECTED_STATE);
        initializeScanner();

        Firebase.setAndroidContext(this);
    }

    @Override
    protected void onStart() {
        mGoogleApiClient.connect();
        mRef = new Firebase("https://bletrackme.firebaseio.com/user/location");

        mRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap location = (HashMap) dataSnapshot.getValue();
//                tvLargeText.setText(location.get("longitude") + " " +location.get("latitude"));
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        startLocationUpdates();

        if (bluetoothAdapter == null) {
            return;
        }

        // Ensures Bluetooth is enabled on the device.  If Bluetooth is not currently enabled,
        // fire an intent to display a dialog asking the user to grant permission to enable it.
        if (!bluetoothAdapter.isEnabled()) {
            bluetoothAdapter.enable();
        }

        adjustScannerOnResume();

        initDangerTextView();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
        if (scanner != null) {
            setScanActive(false);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // User chose not to enable Bluetooth.
        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == Activity.RESULT_CANCELED) {
                finish();
            } else {
                adjustScannerOnResume();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        createLocationRequest();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, PackageManager.PERMISSION_GRANTED);
            return;
        }

        getCurrentLocation();
        mMap.setMyLocationEnabled(true);
    }

    @Override
     public void onConnected(@Nullable Bundle bundle) {
        createLocationRequest();
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "onConnectionSuspended " + i);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(TAG, connectionResult.getErrorMessage());
    }

    @Override
    public void onLocationChanged(Location location) {
        updateCurrentLocation(location);
    }

    private void createGoogleApiClient() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        mGoogleApiClient.connect();
    }

    private void startLocationUpdates() {
        if (mGoogleApiClient.isConnected() && !mRequestingLocationUpdates && !isDisableLocationUpdate) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            mRequestingLocationUpdates = true;
        }
    }

    private void stopLocationUpdates() {
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mRequestingLocationUpdates = false;
        }
    }

    private void getCurrentLocation() {
        if (isDisableLocationUpdate) {
            isDisableLocationUpdate = false;
            startLocationUpdates();
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        updateCurrentLocation(null);
        LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
    }

    public void updateCurrentLocation(Location location){
        if (location != null) {
//            Log.i(TAG, "Latitude:  " + location.getLatitude());
//            Log.i(TAG, "Longitude: " + location.getLongitude());
            jumpToPosition(location.getLatitude(), location.getLongitude());
        } else {
//            Log.i(TAG, "location is null");
        }

    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void jumpToPosition(double lat, double lng) {
        LatLng here = new LatLng(lat, lng);

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(here, 19f));

        mMap.clear();
        if (range < 0){
            mMap.clear();
            return;
        }
        Circle circle = mMap.addCircle(new CircleOptions()
                .center(here)
                .radius(range)
                .strokeColor(Color.argb(255, 34, 139, 34))
                .strokeWidth(3)
                .fillColor(Color.argb(100, 34, 139, 34)));
    }

    private void checkAndInitBluetoothAdapter() {
        final int bleStatus = BleUtils.getBleStatus(getBaseContext());
        switch (bleStatus) {
            case BleUtils.STATUS_BLE_NOT_AVAILABLE:
                new AlertDialog.Builder(this)
                        .setTitle("Not compatible")
                        .setMessage("Bluetooth Low Energy technology is not supported on your device.")
                        .setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                System.exit(0);
                            }
                        })
                        .show();
                return;
            case BleUtils.STATUS_BLUETOOTH_NOT_AVAILABLE:
                new AlertDialog.Builder(this)
                        .setTitle("Not compatible")
                        .setMessage("Bluetooth is not supported on your device.")
                        .setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                System.exit(0);
                            }
                        })
                        .show();
                return;
            default:
                bluetoothAdapter = BleUtils.getBluetoothAdapter(getBaseContext());
        }
    }

    private void turnBluetoothOn() {
        Log.i(TAG, "MainActivity - turn bluetooth on");
        if (!bluetoothAdapter.isEnabled()) {
            Intent turnOn = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(turnOn, 0);
            Toast.makeText(getApplicationContext(), "Turned on", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getApplicationContext(), "Already on", Toast.LENGTH_LONG).show();
        }
    }

    private void adjustScannerOnResume() {

        if (appState != BLE_TRACKING_STATE) {
            setScanActive(true);
        } else {
            setScanActive(false);
        }

    }

    private synchronized void updateAppState(int STATE){
        appState = STATE;
        String state;
        switch(STATE){
            case 0 :
                state = "BLE disconnected";
                break;
            case 1 :
                state = "Connecting";
                break;
            case 2 :
                state = "Tracking by BLE";
                break;
            default :
                state = "unknown state";
        }
        Log.i(TAG, "updated to state: " + state);
    }

    private void initializeScanner() {
        scanner = new BleScanCompat(bluetoothAdapter, new ScanProcessor());
        scanner.setScanPeriod(SCAN_PERIOD);
    }

    private void setScanActive(boolean active) {
        if (active) {
            Log.i(TAG, "start scan");
            scanner.stop();
            scanner.start();
        } else {
            if (scanner.isScanning()){
                scanner.stop();
                Log.i(TAG, "scanner is scanning --> stop it right now");
            } else {
                Log.i(TAG, "scanner is stopped --> do nothing");
            }
        }
    }


    private class ScanProcessor implements BleScanCompat.BleDevicesScannerListener {

        /**
         * Scan map. Holds device which was found on ever scan.
         */
        private final Map<BluetoothDevice, Integer> scanMap = new HashMap<>();

        @Override
        public void onScanStarted() {
            Log.i(TAG, "onScanStarted");
        }

        @Override
        public void onScanRepeat() {
            Log.d(TAG, "on scan repeat");
            updateDevices();
        }

        @Override
        public void onScanStopped() {
            setScanActive(false);
        }

        @Override
        public void onLeScan(final BluetoothDevice device, final int rssi,byte[] bytes) {
            Log.i(TAG, "detected: " + device.getName() + rssi + "dBm");
            scanMap.put(device, rssi);
            updateDevices();

            if (RECORD_DEVICE_NAME.equals(device.getName())) {
                setScanActive(false);
                Log.i(TAG, "start to connect to "+device.getName());
                device.connectGatt(getBaseContext(), false, mGattCallback);
            }
        }

        private synchronized void updateDevices() {

            for (BluetoothDevice device : scanMap.keySet()) {
                final int rssi = scanMap.get(device);
                rssiMap.put(device, rssi);
            }

            scanMap.clear();
        }
    }


    // Gatt Callback
    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                Log.i(TAG, gatt.getDevice() + ": Connected.. ");
                gatt.readRemoteRssi();
                updateAppState(BLE_TRACKING_STATE);
                initSafeTextView();
                return;
            }
            if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                Log.i(TAG, gatt.getDevice() + ": Disconnected.. ");
                setScanActive(true);
                updateAppState(BLE_DISCONECTED_STATE);
                initDangerTextView();
                range = -1f;
                return;
            }
            updateAppState(CONNECTING_STATE);
            updateViewStatus(CONNECTING_STATE);
            Log.i(TAG, "trying to connect: "+ gatt.getDevice());
        }

        public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
            Log.i(TAG, gatt.getDevice().getName() + " RSSI:" + rssi + "db ");
            range = getRange(-65, rssi);
            Log.i(TAG, "----------------------range: " + range);
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            gatt.readRemoteRssi();
        }
    };

    protected double calculateAccuracy(double txPower, double rssi) {
        if (rssi == 0) {
            return -1.0; // if we cannot determine accuracy, return -1.
        }

        double ratio = rssi*1.0/txPower;
        if (ratio < 1.0) {
            return Math.pow(ratio,10);
        }
        else {
            double accuracy =  (0.89976)*Math.pow(ratio,7.7095) + 0.111;
            return accuracy;
        }
    }

    protected double getRange(double txCalibratedPower,double rssi) {
        double ratio_db = txCalibratedPower - rssi;
        double ratio_linear = Math.pow(10, ratio_db / 10);

        double r = Math.sqrt(ratio_linear);
        return r;
    }

    private void updateViewStatus(int STATE) {
        switch (STATE){
            case BLE_DISCONECTED_STATE:
                initDangerTextView();
                break;
            case BLE_TRACKING_STATE:
                break;
            default:
                break;
        }

    }

    public void initDangerTextView(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvStatus.setTextSize(36);
                tvStatus.setText("DANGER\nLOST CONNECTION");
                tvStatus.setBackgroundColor(Color.argb(100,255,0,0));
                tvStatus.setTextColor(Color.argb(255,255,0,0));
                Animation anim = new AlphaAnimation(0.0f, 1.0f);
                anim.setDuration(400);
                anim.setStartOffset(200);
                anim.setRepeatMode(Animation.REVERSE);
                anim.setRepeatCount(Animation.INFINITE);
                tvStatus.startAnimation(anim);
            }
        });

        Log.i(TAG, "setup notify danger ---> done");
    }

    public void initSafeTextView(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvStatus.setTextSize(45);
                tvStatus.setText("SAFE");
                tvStatus.setTextColor(Color.argb(255, 34, 139, 34));
                tvStatus.setBackgroundColor(Color.argb(0, 255, 255, 255));
                tvStatus.clearAnimation();
            }
        });

        Log.i(TAG, "setup notify safe ---> done");
    }

}
